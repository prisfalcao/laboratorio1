﻿using System;

namespace laboratorio1
{
    class Program
    {
        static void Main(string[] args)
        {
            byte b;
            b = byte.MaxValue;
            Console.WriteLine("O valor máximo de byte é " + b + "- usei concatenação nessa linha.");
            Console.WriteLine($"O valor máximo de byte {b}. - usei interpolação nessa linha.");

            int i;
            i = int.MaxValue;
            Console.WriteLine("O valor máximo de int é " + i + "- usei concatenação nessa linha.");
            Console.WriteLine($"O valor máximo de int é {i}. - usei interpolação nessa linha.");

            long l;
            l = long.MaxValue;
            Console.WriteLine("O valor máximo de long é " + l + "- usei concatenação nessa linha.");
            Console.WriteLine($"O valor máximo de long é {l}. Usei interpolação nessa linha.");

            Console.WriteLine("Agora as strings:");

            string strPrimeira = "Alo ";
            string strSegunda = "Mundo";
            string strTerceira = strPrimeira + strSegunda;
            Console.WriteLine(strTerceira);

            int calcTamanho = strTerceira.Length;
            string strQuarta = "Tamanho = " + calcTamanho.ToString();
            Console.WriteLine(strQuarta);
            Console.WriteLine(strTerceira.Substring(0, 5));

            DateTime dt = new DateTime(2020, 09, 16);

            string strQuinta = dt.ToString();
            Console.WriteLine(strQuinta);

            decimal d;
            d = decimal.MinValue;
            Console.WriteLine("O valor mínimo de decimal é " + d + "- usei concatenação nessa linha.");
            Console.WriteLine($"O valor mínimo de decimal {d}. - usei interpolação nessa linha.");

            float f;
            f = float.MinValue;
            Console.WriteLine("O valor mínimo de float é " + f + "- usei concatenação nessa linha.");
            Console.WriteLine($"O valor mínimo de int é {f}. - usei interpolação nessa linha.");

            double dbl;
            dbl = double.MinValue;
            Console.WriteLine("O valor máximo de long é " + dbl + "- usei concatenação nessa linha.");
            Console.WriteLine($"O valor máximo de long é {dbl}. Usei interpolação nessa linha.");

            Console.WriteLine("Agora mais exerciícios com strings:");

            string columns = "Column 1\tColumn 2\tColumn3";
            Console.WriteLine(columns);

            string rows = "Row 1\r\nRow 2\r\nRow 3";
            Console.WriteLine(rows);

            DateTime dow = new DateTime(2020, 9, 16);
        

        }
    }
}
